@extends('layouts.app_layout')

@extends('layouts.section_top_nav')
@extends('layouts.section_sidebar')

@section('title', 'Registration')

@section('styles')
  <!-- Add custom styles here -->

  <!-- JQuery DataTable Css -->
  <link href="{{ asset('adminBSB-master/plugins/jquery-datatable/skin/bootstrap/css/dataTables.bootstrap.css') }}" rel="stylesheet">
  <link href="{{ asset('adminBSB-master/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('section_content')
  <section class="content">
    <div class="container-fluid">
        <!-- Exportable Table -->
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            List of Student Registration
                        </h2>
                        <ul class="header-dropdown m-r--5">
                            <li class="dropdown">
                                <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </a>
                                <ul class="dropdown-menu pull-right">
                                    <li><a href="javascript:void(0);">Action</a></li>
                                    <li><a href="javascript:void(0);">Another action</a></li>
                                    <li><a href="javascript:void(0);">Something else here</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="body">
                        @if (session('message'))
                            <div class="alert alert-{{ session('type') }}">
                            {!! session('message') !!}
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table id="dtIndex" class="table table-bordered table-striped table-hover dataTable js-exportable">
                                <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Place of Birth</th>
                                        <th>Address</th>
                                        <th>School Origin</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tfoot>
                                    <tr>
                                        <th>No</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Place of Birth</th>
                                        <th>Address</th>
                                        <th>School Origin</th>
                                        <th>Actions</th>
                                    </tr>
                                </tfoot>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                        <form id="delete-form" action="{{ route('registration.delete') }}" method="POST" style="display: none;">
                            @csrf
                            <input type="hidden" id="registrationId" name="id" />
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Exportable Table -->
    </div>
  </section>
@endsection

@section('scripts')
  <!-- Jquery DataTable Plugin Js -->
  <script src="{{ asset('adminBSB-master/plugins/jquery-datatable/jquery.dataTables.js') }}"></script>
  <script src="{{ asset('adminBSB-master/plugins/jquery-datatable/skin/bootstrap/js/dataTables.bootstrap.js') }}"></script>
  <script src="{{ asset('adminBSB-master/plugins/jquery-datatable/extensions/export/dataTables.buttons.min.js') }}"></script>
  <script src="{{ asset('adminBSB-master/plugins/jquery-datatable/extensions/export/buttons.flash.min.js') }}"></script>
  <script src="{{ asset('adminBSB-master/plugins/jquery-datatable/extensions/export/jszip.min.js') }}"></script>
  <script src="{{ asset('adminBSB-master/plugins/jquery-datatable/extensions/export/pdfmake.min.js') }}"></script>
  <script src="{{ asset('adminBSB-master/plugins/jquery-datatable/extensions/export/vfs_fonts.js') }}"></script>
  <script src="{{ asset('adminBSB-master/plugins/jquery-datatable/extensions/export/buttons.html5.min.js') }}"></script>
  <script src="{{ asset('adminBSB-master/plugins/jquery-datatable/extensions/export/buttons.print.min.js') }}"></script>
  <script src="{{ asset('adminBSB-master/plugins/sweetalert/sweetalert.min.js') }}"></script>
  
  <!-- Custom Js -->
  <script type="text/javascript">
    $(function () {
      //** Do something here with JQuery **//
    //   $('.js-basic-example').DataTable({
    //     responsive: true
    //   });

      //Exportable table
    //   $('.js-exportable').DataTable({
    //       dom: 'Bfrtip',
    //       responsive: true,
    //       buttons: [
    //           'copy', 'csv', 'excel', 'pdf', 'print'
    //       ]
    //   });

      var dtIndex = $('#dtIndex').DataTable({
        stateSave:true,
        processing: true,
        responsive: true,
        serverSide: true,
        dom: 'Bfrtip',
        buttons: [
            {
                extend: 'excel',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend: 'pdf',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            },
            {
                extend: 'print',
                exportOptions: {
                    columns: [ 0, 1, 2, 3, 4, 5 ]
                }
            }
        ],
        ajax: {
            url: "{!! route('registration.dtIndex') !!}",
            type: "GET",
        },
        columns: [
                { data: 'DT_RowIndex', name: 'DT_RowIndex', width: '5%', className:'text-center' },
                { data: 'name', name: 'name', width: '10%', defaultContent: 'N/A'},
                { data: 'email', name: 'email', width: '15%', defaultContent: 'N/A'},
                { data: 'pob', name: 'pob', width: '15%', defaultContent: 'N/A',
                    render: function (data, type, row) {
                        return  data +", " +row.dob;
                    }
                },
                { data: 'address', name: 'address', width: '15%', defaultContent: 'N/A'},
                { data: 'school_origin', name: 'school_origin', width: '10%', defaultContent: 'N/A'},
                { data: 'id', name: 'id', width: '10%', className: 'text-center',
                    render: function (data, type, row) {
                        // var actEdit   = " <a href='/admin-area/registration/"+data+"/edit' class='btn btn-warning btn-xs waves-effect' title='Edit'><i class='material-icons'>mode_edit</i></a>";
                        var actDelete = " <button id='delete-button' data-id='"+data+"' class='btn btn-danger btn-sm waves-effect delete' title='Delete'><i class='material-icons'>delete</i></button>";
                        
                        return actDelete;
                    }
                }
            ]
        });

        $('body').on('click', '.delete', function() {
            $('#registrationId').val($(this).attr('data-id'));
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover deleted data!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            }, function () {
                $('#delete-form').submit();
            });
        });
    });
  </script>
@endsection