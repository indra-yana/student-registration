<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>@yield('title') | {{ env('APP_NAME') }}</title>
    <!-- Favicon-->
    <link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="{{ asset('adminBSB-master/plugins/bootstrap/css/bootstrap.css') }}" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="{{ asset('adminBSB-master/plugins/node-waves/waves.css') }}" rel="stylesheet" />
    
    <!-- Animation Css -->
    <link href="{{ asset('adminBSB-master/plugins/animate-css/animate.css') }}" rel="stylesheet" />

     <!-- Bootstrap Select Css -->
    <link href="{{ asset('adminBSB-master/plugins/select2/css/select2.min.css') }}" rel="stylesheet" />

    <!-- Bootstrap DatePicker Css -->
    <link href="{{ asset('adminBSB-master/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="{{ asset('adminBSB-master/css/style.css') }}" rel="stylesheet">

    <!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
    <link href="{{ asset('adminBSB-master/css/themes/all-themes.css') }}" rel="stylesheet" />

  </head>

  <body class="signup-page">
    <div class="signup-box">
        <div class="logo">
            <a href="javascript:void(0);">Student<b>Area</b></a>
            <small>Student Registration System</small>
        </div>
        <div class="card">
            <div class="body">
                <form id="sign_up" method="POST" action="{{ route('registration.store') }}">
                    @csrf
                    <div class="msg">Register a new membership</div>
                    @if (session('message'))
                        <div class="alert alert-{{ session('type') }}">
                        {!! session('message') !!}
                        </div>
                    @endif
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line @if ($errors->form->first('name')) error focused @endif">
                            <input type="text" class="form-control" name="name" placeholder="Full Name" value="{{ old('name') }}" required autofocus autocomplete="off">
                        </div>
                        @if ($errors->form->first('name'))
                            <label class="error" for="name">{{ $errors->form->first('name') }}</label>
                        @endif
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">email</i>
                        </span>
                        <div class="form-line @if ($errors->form->first('email')) error focused @endif">
                            <input type="email" class="form-control" name="email" placeholder="Email Address" value="{{ old('email') }}" required autocomplete="off">
                        </div>
                        @if ($errors->form->first('email'))
                            <label class="error" for="email">{{ $errors->form->first('email') }}</label>
                        @endif
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person_pin_circle</i>
                        </span>
                        <div class="form-line @if ($errors->form->first('pob')) error focused @endif">
                            <input type="text" class="form-control" name="pob" placeholder="Place of Birth" value="{{ old('pob') }}" required>
                        </div>
                        @if ($errors->form->first('pob'))
                            <label class="error" for="pob">{{ $errors->form->first('pob') }}</label>
                        @endif
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">calendar_today</i>
                        </span>
                        <div class="form-line @if ($errors->form->first('dob')) error focused @endif" id="bs_datepicker_container">
                            <input type="text" class="form-control" name="dob" placeholder="Date of Birth (YYYY-MM-DD)" value="{{ old('dob') }}" data-date-format="yyyy-mm-dd" data-date-end-date="0d" required autocomplete="off">
                        </div>
                        @if ($errors->form->first('dob'))
                            <label class="error" for="dob">{{ $errors->form->first('dob') }}</label>
                        @endif
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">map</i>
                        </span>
                        <select class="form-control show-tick @if ($errors->form->first('address_province')) error focused @endif" name="address_province" id="address_province" onchange="getCity(this.value)" required>
                            <option value="">-- Please select province --</option>
                            @foreach($province as $value)
                            <option value="{{ $value->id }}|{{ $value->nama }}" {{ old('address_province') == $value->id.'|'.$value->nama ? 'selected' : '' }}>{{ $value->nama }}</option>
                            @endforeach
                        </select>
                        @if ($errors->form->first('address_province'))
                            <label class="error" for="address_province">{{ $errors->form->first('address_province') }}</label>
                        @endif
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">location_city</i>
                        </span>
                        <select class="form-control show-tick @if ($errors->form->first('address_city')) error focused @endif" name="address_city" id="address_city" required>
                            <option value="">-- Please select city --</option>
                        </select>
                        @if ($errors->form->first('address_city'))
                            <label class="error" for="address_city">{{ $errors->form->first('address_city') }}</label>
                        @endif
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">school</i>
                        </span>
                        <div class="form-line @if ($errors->form->first('school_origin')) error focused @endif">
                            <input type="text" class="form-control" name="school_origin" placeholder="School Origin (Sekolah Asal)" value="{{ old('school_origin') }}"  required>
                        </div>
                        @if ($errors->form->first('school_origin'))
                            <label class="error" for="school_origin">{{ $errors->form->first('school_origin') }}</label>
                        @endif
                    </div>

                    <!-- <div class="form-group">
                        <input type="checkbox" name="terms" id="terms" class="filled-in chk-col-pink">
                        <label for="terms">I read and agree to the <a href="javascript:void(0);">terms of usage</a>.</label>
                    </div> -->

                    <button class="btn btn-block btn-lg bg-pink waves-effect" type="submit">REGISTER</button>

                    <div class="m-t-25 m-b--5 align-center">
                        <!-- <a href="">You already have a membership?</a> -->
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery Core Js -->
    <script src="{{ asset('adminBSB-master/plugins/jquery/jquery.min.js') }}"></script>

    <!-- Bootstrap Core Js -->
    <script src="{{ asset('adminBSB-master/plugins/bootstrap/js/bootstrap.js') }}"></script>

    <!-- Waves Effect Plugin Js -->
    <script src="{{ asset('adminBSB-master/plugins/node-waves/waves.js') }}"></script>

    <!-- Validation Plugin Js -->
    <script src="{{ asset('adminBSB-master/plugins/jquery-validation/jquery.validate.js') }}"></script>

    <!-- Select Plugin Js -->
    <script src="{{ asset('adminBSB-master/plugins/select2/js/select2.min.js') }}"></script>

    <!-- Moment Plugin Js -->
    <script src="{{ asset('adminBSB-master/plugins/momentjs/moment.js') }}"></script>

    <!-- Bootstrap Datepicker Plugin Js -->
    <script src="{{ asset('adminBSB-master/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>

    <script type="text/javascript">
      $(function () {
        $('#bs_datepicker_container input').datepicker({
            autoclose: true,
            todayHighlight: 'linked',
            clearBtn: true,
            container: '#bs_datepicker_container'
        }).keydown(false);

        $('#address_province').select2({});
        $('#address_city').select2({});

        @if(old('address_province'))
            getCity( "{{ old('address_province') }}" );
        @endif

        // $('#sign_up').validate({
        //   rules: {
        //       'terms': {
        //           required: true
        //       },
        //       'confirm': {
        //           equalTo: '[name="password"]'
        //       }
        //   },
        //   highlight: function (input) {
        //       console.log(input);
        //       $(input).parents('.form-line').addClass('error');
        //   },
        //   unhighlight: function (input) {
        //       $(input).parents('.form-line').removeClass('error');
        //   },
        //   errorPlacement: function (error, element) {
        //       $(element).parents('.input-group').append(error);
        //       $(element).parents('.form-group').append(error);
        //   }
        // });
      });

      function getCity(id) {
            id = id.split("|");
            $('#address_city').val('');
            $('#address_city').empty();
            $('#address_city').append('<option value="">-- Please select city --</option>');

            $.ajax({
        	    url: "{{ $api_city }}"+id[0],
        	    type: 'GET',
        	    contentType: 'application/json',
        	    dataType: 'json',
        	}).success(function(data) {
                for (var i = 0; i < data.kota_kabupaten.length; i++) {
                    var value = data.kota_kabupaten[i].id+'|'+data.kota_kabupaten[i].nama;
                    var selected = value == "{{ old('address_city') }}" ? ' selected ' : '';

                    $('#address_city').append('<option value="'+value+'"' +selected +'>'+data.kota_kabupaten[i].nama+'</option>');
                }

                $('#address_city').trigger('change');
        	});
	    }
    </script>
  </body>

</html>