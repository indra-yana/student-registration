<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Registration;
use Validator;
use Auth;

define("API_PROVINCE", "https://dev.farizdotid.com/api/daerahindonesia/provinsi");
define("API_CITY", "https://dev.farizdotid.com/api/daerahindonesia/kota?id_provinsi=");

class RegistrationController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['registrationStudent', 'store', 'getRegisteredStudent']]);
    }

    public function registrationStudent()
    {
        $html_site = json_decode( file_get_contents(API_PROVINCE) );
        $province = $html_site->provinsi;
        $api_city = API_CITY;
        
        return view('registration.registration', compact('province', 'api_city'));
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'string|required',
            'email' => 'email|required|unique:registration,email',
            'pob' => 'string|required',
            'dob' => 'required|date|date_format:Y-m-d',
            'address_province' => 'string|required',
            'address_city' => 'string|required',
            'school_origin' => 'string|required',
        ]);

        if ($validator->fails()) {
            return redirect()
                    ->route('registration.student')
                    ->withErrors($validator, 'form')
                    ->withInput()
                    ->with('type', 'danger')
                    ->with('message', '<b>Oh!</b> Please ensure to fill all fields correctly and re-submit the form.');
        }

        $registration = new Registration();
        $registration->name = ucwords($request->name);
        $registration->email = $request->email;
        $registration->pob = ucwords($request->pob);
        $registration->dob = $request->dob;
        $registration->address = explode("|", $request->address_province)[1] .", " .explode("|", $request->address_city)[1];
        $registration->school_origin = $request->school_origin;

        if (!$registration->save()) {
            return redirect()
                    ->route('registration.student')
                    ->with('type', 'danger')
                    ->with('message', '<b>Oh!</b> There\'s something wrong with the system. Please contact administrator about this.');
        }

        return redirect()
                ->route('registration.student')
                ->with('type', 'success')
                ->with('message', '<b>Well Done!</b> The data has been successfully saved.');
    }

    public function index()
    {
        return view('registration.index');
    }

    public function delete(Request $request)
    {
        $registration = Registration::findOrFail($request->get('id'));

        if (!$registration->delete()) {
            return redirect()
                ->route('registration.index')
                ->with('type', 'danger')
                ->with('message', '<b>Oh!</b> There\'s something wrong with the system. Please contact administrator about this.');
        }

        return redirect()
                ->route('registration.index')
                ->with('type', 'success')
                ->with('message', '<b>Well Done!</b> The data has been successfully deleted.');
    }

    public function dtIndex(Request $request)
    {
        return datatables()->of(Registration::all())->addIndexColumn()->toJson();  
    }

    public function getRegisteredStudent() {
        $student = Registration::all();

        return response()->json([
            'success' => true,
            'data' => $student,
        ]);
    }
}
