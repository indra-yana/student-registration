## Aplikasi System Registrasi Mahasiswa Sederhana

Aplikasi Registrasi Mahasiswa sederhana berbasis website dengan menggunakan Framework Laravel 5.8

## Fitur:
- Form registrasi mahasiswa
- Simple panel admin
- Data Validation
- Datatable Server Side
- Data provinsi dan kota menggunakan API
- Simple API 
```json
{
   "success":true,
   "data":[
      {
         "id":1,
         "name":"Indra Muliana",
         "email":"indra.ndra26@gmail.com",
         "pob":"Bandung",
         "dob":"1994-12-26",
         "address":"Jawa Barat, Kabupaten Bandung Barat",
         "school_origin":"UNJANI",
         "created_at":"2021-05-04 03:42:53",
         "updated_at":"2021-05-04 03:42:53"
      }
   ]
}
```

## Server Requirment
- PHP >= 7.2.3
- XAMPP
- Composer Versi 1.10.X
- Untuk menghindari error composer ketika menginstall package: Try increasing the memory_limit in your php.ini file (ex. /etc/php5/cli/php.ini for Debian-like or windows C:\Program Files\PHP\php-7.X\php.ini) lebih lengkap (klik [disini](https://getcomposer.org/doc/articles/troubleshooting.md#memory-limit-errors))
- untuk requirement laravel 5.8 lebih lengkap dapat dilihat pada link berikut (klik [disini](https://laravel.com/docs/5.8#server-requirements)).

## Cara Install
- Clone project.
- Buka CMD dan masuk pada directory aplikasi
- Ketikkan "composer install"
- Start service MySQL pada XAMPP
- Buat database baru dengan nama "student_registration_db" di PHPMyAdmin
- Copy .env.example dengan perintah cp .env.example .env
- Setting database pada file .env
- Contoh:
    - DB_CONNECTION=mysql
    - DB_HOST=127.0.0.1
    - DB_PORT=3306
    - DB_DATABASE=student_registration_db
    - DB_USERNAME=root
    - DB_PASSWORD=
- Ketikkan "php artisan key:generate"
- Ketikkan "php artisan migrate"
- Ketikkan "php artisan db:seed --class=UsersTableSeeder"
- Ketikkan "php artisan serve"
- Selesai

## User Admin Login
**Email:**
admin@laravel.com
**Password:**
secret

## WEB ROUTE
- /registration
- /admin-area
- /api/v1/registered-student

## Screenshots
[![Screenshot](public/adminBSB-master/images/student-area.PNG)](#)

[![Screenshot](public/adminBSB-master/images/admin-area-login.PNG)](#)

[![Screenshot](public/adminBSB-master/images/admin-area-registration.PNG)](#)


