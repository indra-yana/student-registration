<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'name' => 'Administrator',
            	'email' => 'admin@laravel.com',
                'password' => bcrypt('secret'),
                'created_at' => Date("Y-m-d h:i:s"),
                'updated_at' => Date("Y-m-d h:i:s"),
            ]
        ]);
    }
}
